package main;
import java.util.HashMap;
import java.util.Map;

class SymStats
{
	//поля
	private Map<Character, Integer> m_map;  
	
	
   // Создает класс для подсчета статистики использования указанных 
   // символов
   public SymStats(char[] charsForStats)
   {
	   m_map = new HashMap<Character, Integer>();
	   for (int i=0; i<charsForStats.length; i++)
		   m_map.put(charsForStats[i], 0);
   }
// Вычисляет статистику использования символов в данной строке
   public void calculate(String s)
   {
	   for (int i=0; i<s.length(); i++)
		   if (m_map.get(s.charAt(i)) != null) m_map.put(s.charAt(i), m_map.get(s.charAt(i))+1);
	   writeStats();
   }

   // Возвращает количество вхождений указанного символа
   public int countChar(char c)
   {
	   return m_map.get(c);		   
   }

   // Возвращает общее число подсчитываемых символов
   public int countTotal()
   {
	   return m_map.size();
	   
   }

   // Выдает подсчитанную статистику на экран
   public void writeStats()
   {
	   for (Map.Entry<Character, Integer> entry: m_map.entrySet())
		   System.out.println(entry.getKey()+": "+ entry.getValue());
   }
}