package main;
import java.util.Scanner;

import main.SymStats;

public class Solve {

	public static void main(String[] args) {
		char[] alpha = {'а', 'е', 'ё','и','о','у','ы','э', 'ю','я'};
		SymStats MyStat = new SymStats(alpha);
		Scanner in = new Scanner(System.in);
		MyStat.calculate(in.nextLine());
		in.close();
	}

}
