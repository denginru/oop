
import org.junit.Test;

import vector3D.Segment;
import vector3D.Vector;
import vector3D.Vector3D;

import static org.junit.Assert.*;

public class Test_1 {

	@Test
	public void testScalar()
	{
		final double[] coords1 = {1.0, 2.0, 3.0};
		final double[] coords2 = {3.0, 2.0, 1.0};
		final Vector3D v1 = new Vector3D(coords1);
		final Vector3D v2 = new Vector3D(coords2);
		final double res = v1.scalar(v2);
		assertEquals((double)10.0, res, 0.0001);		//expected, actual, epsilon. Epsilon is need because of comparing double numbers
	}
	
	@Test
	public void testLen()
	{
		final double[] coords1 = {1.0, 2.0, 3.0};
		final Vector3D v1 = new Vector3D(coords1);
		double res = v1.len();
		assertEquals((double)3.741657387, res, 0.00001);		//expected, actual, epsilon. Epsilon is need because of comparing double numbers
		final double[] coords2 = {3.0, 2.0, 1.0};
		final Vector3D v2 = new Vector3D(coords2);
		res = v2.len();
		assertEquals((double)3.741657387, res, 0.00001);		//expected, actual, epsilon. Epsilon is need because of comparing double numbers	}
	}
	
	@Test
	public void testAdd() {
		final double[] coords1 = {1.0, 2.0, 3.0};
		final Vector3D v1 = new Vector3D(coords1);
		final double[] coords2 = {3.0, 2.0, 1.0};
		final Vector3D v2 = new Vector3D(coords2);
		double[] expected = {4, 4, 4};
		Vector res = v1.add(v2);
		for (int i=0; i<v1.dimension(); i++)
			assertEquals(expected[i], res.getComponent(i), 0.00001);
	}
	
	@Test
	public void testSub() {
		final double[] coords1 = {1.0, 2.0, 3.0};
		final Vector3D v1 = new Vector3D(coords1);
		final double[] coords2 = {3.0, 2.0, 1.0};
		final Vector3D v2 = new Vector3D(coords2);
		double[] expected = {-2, 0, 2};
		Vector res = v1.sub(v2);
		for (int i=0; i<v1.dimension(); i++)
			assertEquals(expected[i], res.getComponent(i), 0.00001);
	}
	
	@Test
	public void testMultiply() {
		final double[] coords1 = {1.0, 2.0, 3.0};
		final Vector3D v1 = new Vector3D(coords1);
		double[] expected = {5, 10, 15};
		Vector res = v1.multiply(5);
		for (int i=0; i<v1.dimension(); i++)
			assertEquals(expected[i], res.getComponent(i), 0.00001);
	}
	
	@Test
	public void testDistToPoint()
	{
		final double[] c1 = {0, 2, 0};
		final double[] c2 = {3, 2, 0};
		final double[] c3 = {2, 1, 0}; 
		Vector3D point = new Vector3D(c3);
		Vector3D start = new Vector3D(c1);
		Vector3D end = new Vector3D(c2);
		final Segment s = new Segment(start, end);
		double r = s.distance(point);
		assertEquals(1, r, 0.0001);
	}
	
	
}
