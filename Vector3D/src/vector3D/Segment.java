package vector3D;

public class Segment {
	private Vector3D m_Start;
	private Vector3D m_End;
	
	public Segment(Vector3D start, Vector3D end)
	{
		m_Start = start;
		m_End = end;
	}
	
	
	public double len()
	{
		double res = 0;
		for (int i = 0; i<m_Start.dimension(); i++)
			res += Math.pow(m_End.getComponent(i) - m_Start.getComponent(i), 2);
		return Math.sqrt(res);	
	}
	
	public double distance(Vector3D point)
	{
		double r1 = new Segment(this.m_Start, point).len();
		double r2 = new Segment(m_End, point).len();
		
		double a = m_Start.getComponent(1) - m_End.getComponent(1);
		double b = m_End.getComponent(0) - m_Start.getComponent(0);
		double c = m_Start.getComponent(0)*m_End.getComponent(1) - m_Start.getComponent(1)*m_End.getComponent(0);
		
		double r3 = Math.abs(a*point.getComponent(0) + b*point.getComponent(1) + c)/Math.sqrt(a*a+b*b);
		
		return Math.min(Math.min(r1, r2), r3);
	}
}
