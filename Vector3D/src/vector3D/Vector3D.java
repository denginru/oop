package vector3D;

import java.lang.Math;

public final class Vector3D implements Vector {
	
	double m_coordinates[] = new double[dimension()];

	public Vector3D(double[] coordinates) {
		m_coordinates = coordinates.clone();
	}
	
	@Override
	public int dimension() {
		return 3;
	}

	@Override
	public double getComponent(int i) {
		return m_coordinates[i];
	}

	@Override
	public double scalar(Vector v) {
		double res = 0;
		for (int i = 0; i< dimension(); i++)
			res += v.getComponent(i)*this.getComponent(i);
		return res;
	}



	@Override
	public Vector multiply(double factor) {
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i)*factor;
		Vector3D returned = new Vector3D(coord);
		return returned;
	}

	@Override
	public Vector add(Vector v) {
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i) + v.getComponent(i);
		Vector3D returned = new Vector3D(coord);
		return returned;
	}

	@Override
	public Vector sub(Vector v) {
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i) - v.getComponent(i);
		Vector3D returned = new Vector3D(coord);
		return returned;
	}
	
	@Override
	public double len() {
		double res = 0;
		for (int i = 0; i < dimension(); i++)
			res += Math.pow(this.getComponent(i), 2);
		return Math.sqrt(res);
	}
}
