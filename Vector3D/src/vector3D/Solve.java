package vector3D;

public class Solve {

	public static void main(String[] args) {
		final double[] coords1 = {1.0, 2.0, 3.0};
		final double[] coords2 = {3.0, 2.0, 1.0};
		final Vector3D v1 = new Vector3D(coords1);
		final Vector3D v2 = new Vector3D(coords2);
		final double res = v1.scalar(v2);
		System.out.println(res);
	}

}
