import java.util.Objects;

/**
 * Created by roman on 21.04.16.
 */
public class Join extends MyPair {
    public Join(Object firstElement, Object secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public boolean equals(Object o) {
            if (o instanceof MyPair)
                if (o != null)
                    return Objects.equals(((MyPair) o).getFirstElement(),getFirstElement()) && Objects.equals(((MyPair) o).getSecondElement(),getSecondElement()) ||
                            Objects.equals(((MyPair) o).getFirstElement(),getSecondElement()) && Objects.equals(((MyPair) o).getSecondElement(),getFirstElement());
                else
                    return  false;
            else
                return false;
    }
    @Override
    public int hashCode()
    {
        int result = Objects.hashCode(getFirstElement());
        result = 31 * result + getSecondElement().hashCode();
        return result;
    }
}
