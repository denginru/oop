import java.util.Objects;

/**
 * Created by roman on 21.04.16.
 */
public class MyPair <A, B> {
    private A firstElement;
    private B secondElement;

    public MyPair(A firstElement, B secondElement) {
        this.firstElement = firstElement;
        this.secondElement = secondElement;
    }


    @Override
    public boolean equals(Object o) {
        MyPair<?, ?> myPair = (MyPair<?, ?>) o;
        return Objects.equals(firstElement,myPair.firstElement);
    }

    @Override
    public int hashCode() {
        int result = 13 * Objects.hashCode(firstElement);
        result = 31 * result + secondElement.hashCode();
        return result;
    }

    public A getFirstElement() {
        return firstElement;
    }

    public void setFirstElement(A firstElement) {
        this.firstElement = firstElement;
    }

    public B getSecondElement() {
        return secondElement;
    }

    public void setSecondElement(B secondElement) {
        this.secondElement = secondElement;
    }
}