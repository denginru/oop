import java.util.Objects;

/**
 * Created by roman on 21.04.16.
 */
public class Entry extends  MyPair{
    public Entry(Object firstElement, Object secondElement) {
        super(firstElement, secondElement);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MyPair && o != null)
            return Objects.equals(((MyPair) o).getFirstElement(), getFirstElement());
        else
            return false;
    }

    @Override
    public int hashCode()
    {
        return getFirstElement().hashCode();
    }


}
