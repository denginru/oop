import java.io.*;
import java.util.logging.Logger;

/**
 * Created by roman on 24.05.16.
 */
public class coder {
    private String inFileName;
    private String outFileName;
    private String inCode;

    public coder(String inFileName, String outFileName, String inCode, String outCode) {
        this.inFileName = inFileName;
        this.outFileName = outFileName;
        this.inCode = inCode;
        this.outCode = outCode;
    }

    private String outCode;



    public void Rewrite()
    {
        try
        {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(
                            new FileInputStream(inFileName), inCode));
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(
                            new FileOutputStream(outFileName), outCode));
            int nextChar;

            while ((nextChar= reader.read()) > 0)
            {
                writer.write(nextChar);
            }
            writer.flush();
            writer.close();
        }

        catch (UnsupportedEncodingException ex)
        {
            System.out.println("Неподдерживаемая кодировка");
        }

        catch (FileNotFoundException ex)
        {
            System.out.println("Файл не найден");
        }

        catch (IOException ex)
        {
            System.out.println("Ошибка ввода/вывода");
        }
    }
}
