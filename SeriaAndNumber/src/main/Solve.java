package main;

import java.util.Scanner;

public class Solve {
	
	/*
	 * В условии задачи сказано, что серия и номер заключены между пробелами.
	 * Считаем, что в конце строки тоже стоит пробел 
	*/
	
	public static String FindSeria(String s)
	{
		String ser = "серия";
		int start = -1;
		int end = -1;
		
		for (int i = 0; i<s.length()-ser.length(); i++)
			if (s.substring(i, i+ser.length()).toLowerCase().equals(ser.toLowerCase()))
			{
				start = i + ser.length()+1;
				break;
			}
		if (start == -1) return "";
				
		for (int i = start; i<s.length(); i++)
		{
			end = i;
			if (s.charAt(i) == ' ') break;
		}
		
		return s.substring(start, end);
	}
	
	public static String FindNum(String s)
	{
		int start = -1;
		int end = -1;
		
		for (int i = 0; i<s.length(); i++)
			if (s.charAt(i) == '№') {start = i+2; break;}
		
		if (start == -1) return "";
		
		for (int i = start; i<s.length(); i++)
		{
			end = i;
			if (s.charAt(i) == ' ') break;
		}
		
		return s.substring(start, end);
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		String seria = FindSeria(s);
		String number = FindNum(s);
		if (!seria.equals("")) System.out.println("Серия: " + seria);
		if (!number.equals("")) System.out.println("Номер " + number);
	}

}