/*
 * Roman Dengin. MT-301
 */

package BracketBalanceRight;
import java.util.Scanner;

import BracketBalanceRight.BracketBalanceChecker;

public class Solve {

	public static void main(String[] args) {
		BracketBalanceChecker bc = new BracketBalanceChecker();
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		bc.IsValis(s);
		System.out.println(bc.GetErrorMessage());
		in.close();
	}

}
