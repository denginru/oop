/*
 * Roman Dengin. MT-301
 */

package BracketBalanceRight;

import java.util.Stack;

public class BracketBalanceChecker {
	private int 				m_errorPos;
	private Stack<Character>	m_openBrackets;
 	
	public BracketBalanceChecker()
	{
		m_openBrackets = new Stack<Character>();
		m_errorPos = -1;
	}

	
	private boolean CheckMatching(char charAt) {
		if (m_openBrackets.isEmpty()) return false;
		boolean res = ( ( charAt == ')' ) && ( m_openBrackets.lastElement() == '(' ) ) ||
						( ( charAt == ']' ) && ( m_openBrackets.lastElement() == '[' ) ) ||
						( ( charAt == '}' ) && ( m_openBrackets.lastElement() == '{' ) ) ||
						( ( charAt == '>' ) && ( m_openBrackets.lastElement() == '<' ) );
		if (res) m_openBrackets.pop();
		return res;
	}

	private boolean IsClosedBracket(char charAt) {
		return ( ( charAt == ')' ) || ( charAt == ']' ) || ( charAt == '}' ) || ( charAt == '>' ) );
	}

	private boolean IsOpenBracket(char charAt) {
		return ( ( charAt == '(' ) || ( charAt == '[' ) || ( charAt == '{' ) || ( charAt == '<' ) );
	}
	
	
	public Boolean IsValis(String s)
	{
		for (int i=0; i<s.length(); i++)
		{
			if ( IsOpenBracket( s.charAt(i) ) ) m_openBrackets.push(s.charAt(i));
			else
				if (IsClosedBracket(s.charAt(i))) 
					if (!CheckMatching(s.charAt(i)))
					{
						m_errorPos = i;
						return false;
					}
		}
		if (!m_openBrackets.isEmpty())
		{
			m_errorPos=s.length()-1;
			return false;
		}
		else
			return true;
	}

	public int GetErrorPos()
	{
		return m_errorPos;
	}
	
	public String GetErrorMessage()
	{
		if (m_errorPos != -1)
			return "On " + m_errorPos + " does not right bracket";
		else
			return "All ok";
	}

}
