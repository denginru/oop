package my.collections;

import java.util.Iterator;

/**
 * Created by roman on 12.05.16.
 */
public class myQueue<T> implements Iterable {
    private T array[];
    private int count;

    public myQueue(int size)
    {
        this.array = (T[]) new Object[size];
        count = 0;
    }

    public boolean IsEmpty()
    {
        return count == 0;
    }

    public void push(T elem)
    {
        if (count == array.length)
            resize();
        else
        {
            array[count] = elem;
            count++;
        }
    }

    public T pop() throws Exception {
        T ret = top();
        for (int i=1;i<array.length;i++)
            array[i-1] = array[i];
        return ret;
    }

    public T top() throws Exception {
        if (!IsEmpty())
        {
            T returnedValue = array[count - 1];
            return returnedValue;
        }
        else
            throw new Exception("Empty queue");
    }

    public void resize()
    {
        T res[] = (T[]) new Object[2*count];
        System.arraycopy(array, 0, res, 0,count);
        array = res;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            int currentIndex = 0;
            @Override
            public boolean hasNext() {
                return count > 0;
            }

            @Override
            public T next() {
                currentIndex++;
                return array[currentIndex-1];
            }
        };
    }
}
