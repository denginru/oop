package my.collections;

import java.util.Iterator;

/**
 * Created by roman on 22.05.16.
 */
public class DinamicArray<T> implements Iterable {
    private T m_array[];
    private int count;
    private int max_count;
    private final int default_start_count = 10;
    private int currentIndex = 0;

    public DinamicArray() {
        max_count = default_start_count;
        count = 0;
        m_array = (T[]) new Object[max_count];;
    }

    public DinamicArray(T array[])
    {
        max_count = array.length;
        count = max_count;
        m_array = (T[]) new Object[array.length];
        System.arraycopy(array, 0, m_array, 0, array.length);
    }

    public int count()
    {
        return count;
    }

    public T get(int index)
    {
        try
        {
            if (index >= count) throw new Exception("index is out of bound");
            return m_array[index];
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
            return null;
        }
    }

    public void set(int index, T value)
    {
        try
        {
            if (index >= count) throw new Exception("index is out of bound");
            m_array[index] = value;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void insertAd(int index, T value)
    {
        try
        {
            if (index >= count) throw new Exception("index is out of bound");
            int newLength;
            if (count + 1 > max_count) newLength = 2 * max_count;
            else newLength = max_count;
            T res[] = (T[]) new Object[newLength];
            for (int i=0;i<index;i++)
                res[i]=m_array[i];
            res[index]=value;
            for (int i=index;i<m_array.length; i++)
                res[i+1]=m_array[i];
            max_count = newLength;
            count++;
            m_array = res;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void deleteAd(int index) {
        try
        {
            if (index >= count) throw new Exception("index is out of bound");
            for (int i=index+1;i<m_array.length; i++)
                m_array[i-1]=m_array[i];
            count--;
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }

    public void add(T value)
    {
        if (count >= max_count) resize();
        m_array[count] = value;
        count++;
    }

    private void resize()
    {
        T res[] = (T[]) new Object[2*count];
        System.arraycopy(m_array, 0, res, 0,count);
        m_array = res;
    }

    @Override
    public Iterator<T> iterator() {
        currentIndex = 0;
        int array_length = count;
        T[] r = m_array;

        Iterator<T> it = new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return currentIndex < array_length;
            }

            @Override
            public T next() {
                currentIndex++;
                return r[currentIndex - 1];
            }
            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }

        };
        return it;
    }
}
