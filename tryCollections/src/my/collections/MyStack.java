package my.collections;

import java.util.Iterator;

/**
 * Created by roman on 12.05.16.
 */
public class MyStack<T extends Object> implements Iterable {
    private T array[];
    private int count;

    public MyStack(int size)
    {
        this.array = (T[]) new Object[size];
        count = 0;
    }

    public void push(T elem)
    {
        if (count == array.length)
            resize();
        array[count] = elem;
        count++;
    }

    public T top() throws Exception {
        if (count != 0) {
            T returnedValue = array[count - 1];
            return returnedValue;
        }
        else
            throw new Exception("stack is empty!");
    }

    public int size()
    {
        return count;
    }

    public void resize()
    {
        T res[] = (T[]) new Object[2*count];
        System.arraycopy(array, 0, res, 0,count);
        array = res;
    }

    public void pop() throws Exception
    {
        if (count != 0)
            count--;
        else
            throw new Exception("Stack is empty");
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return (count > 0);
            }

            @Override
            public T next() {
                return array[count - 1];
            }
        };
    }
}
