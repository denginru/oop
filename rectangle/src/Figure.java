/**
 * Created by roman on 24.05.16.
 */
public interface Figure {
    public double perimetr();
    public double square();
    public String  getName();
}
