/**
 * Created by roman on 24.05.16.
 */
public class Solve {
    public static void main(String[] agrs)
    {
        try {
            Rectangle rectangle = new Rectangle(3, 8);
            System.out.println(rectangle.radiusOutsideCircle());
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
