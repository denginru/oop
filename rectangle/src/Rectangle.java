/**
 * Created by roman on 24.05.16.
 */
public class Rectangle implements Figure{

    private double width;
    private double height;

    @Override
    public String getName() {
        return "Rectangle";
    }

    public Rectangle(double width, double height) throws Exception {
        if (width <= 0 || height <= 0) throw new Exception("Длины сторон должны быть положительными");
        this.width = width;
        this.height = height;
    }

    @Override
    public double perimetr() {
        return 2 * (width + height);
    }

    @Override
    public double square() {
        return width * height;
    }

    public double radiusOutsideCircle()
    {
        return Math.sqrt(width * width + height * height) / 2;
    }
}
