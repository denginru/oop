package calculator;


public class OperationNotSupportedException extends Exception {
	public OperationNotSupportedException() {
		super("Операция не поддерживается");
	}
	public OperationNotSupportedException(String operation) {
		super(operation);
	}

}
