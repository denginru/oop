import calculator.AbstractValueParser;
import datatypes.complex.ComplexValueParser;
import datatypes.integer.IntegerValueParser;
import datatypes.rational.RationalValueParser;
import datatypes.real.RealValueParser;
import datatypes.vector.VectorValueParser;

import java.awt.BorderLayout;




import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;


public class SimpleFrame {
    public static void main(String [] args) {
        JFrame frame=new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.add(new JLabel("<html><font color=blue>Север сверху"),BorderLayout.NORTH);
        JTextArea text=new JTextArea("По умолчанию в центре");
        text.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        frame.add(text);
        frame.add(new JLabel("<html><font color=red>Юг внизу"),BorderLayout.SOUTH);

        frame.add(createButtonPanel(text),BorderLayout.EAST);
        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);


    }

    private static JPanel createButtonPanel(JTextArea text) {
        JPanel result=new JPanel();
        result.setLayout(new BoxLayout(result, BoxLayout.Y_AXIS));
        ActionListener onClick=new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText(e.getActionCommand());
            }
        };
        JButton button1=new JButton("А");
        button1.addActionListener(onClick);
        result.add(button1);
        JButton button2=new JButton("Мы");
        button2.addActionListener(onClick);
        result.add(button2);
        JButton button3=new JButton("Столбиком");
        button3.addActionListener(onClick);
        result.add(button3);
        return result;
    }
}