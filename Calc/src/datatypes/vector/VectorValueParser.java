package datatypes.vector;

import calculator.AbstractValue;
import calculator.AbstractValueParser;
import calculator.DivisionByZeroException;
import calculator.ParseValueException;

/**
 * Created by roman on 05.05.2016.
 */
public class VectorValueParser implements AbstractValueParser {
    @Override
    public VectorValue parse(String value) throws ParseValueException{
        String[] nums = value.split("\\{|\\}|\\,|\\s");
        int notEmtptyCount = 0;
        for (int i=0; i<nums.length; i++) if (!nums[i].equals("")) notEmtptyCount++;
        double[] d = new double[notEmtptyCount];
        try {
            int coordIndex = 0;
            for (int i = 0; i < nums.length; i++) {
                if (!nums[i].equals(""))
                {d[coordIndex] = Double.parseDouble(nums[i]); coordIndex++;}
            }
            return new VectorValue(new Vector3D(d));
        }
        catch (Exception ex)
        {
            throw new ParseValueException();
        }
    }

    @Override
    public String getDatatypeName() {
        return "Вектор";
    }
}
