package datatypes.vector;

import java.lang.Math;
import calculator.*;

public final class Vector3D implements Vector {
	
	double m_coordinates[] = new double[dimension()];
	int size;
	public Vector3D(double[] coordinates) {
		size = coordinates.length;
		m_coordinates = coordinates.clone();
	}
	
	@Override
	public int dimension() {
		return size;
	}

	@Override
	public double getComponent(int i) {
		return m_coordinates[i];
	}

	@Override
	public double scalar(Vector v) throws OperationNotSupportedException {
        if (v.dimension() != dimension()) throw new OperationNotSupportedException("length of vectors are different");
		double res = 0;
		for (int i = 0; i< dimension(); i++)
			res += v.getComponent(i)*this.getComponent(i);
		return res;
	}



	@Override
	public Vector3D multiply(double factor) throws OperationNotSupportedException {
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i)*factor;
		Vector3D returned = new Vector3D(coord);
		return returned;
	}

	@Override
	public Vector3D add(Vector v) throws OperationNotSupportedException {
        if (v.dimension() != dimension()) throw new OperationNotSupportedException("length of vectors are different");
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i) + v.getComponent(i);
		Vector3D returned = new Vector3D(coord);
		return returned;
	}

	@Override
	public Vector3D sub(Vector v) throws OperationNotSupportedException{
        if (v.dimension() != dimension()) throw new OperationNotSupportedException("length of vectors are different");
		double[] coord = new double[dimension()];
		for (int i = 0; i<dimension(); i++)
			coord[i] = this.getComponent(i) - v.getComponent(i);
		Vector3D returned = new Vector3D(coord);
		return returned;
	}
	
	@Override
	public double len() {
		double res = 0;
		for (int i = 0; i < dimension(); i++)
			res += Math.pow(this.getComponent(i), 2);
		return Math.sqrt(res);
	}
}
