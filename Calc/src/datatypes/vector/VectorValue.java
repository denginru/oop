package datatypes.vector;
import calculator.*;
import datatypes.real.RealValue;

/**
 * Created by roman on 05.05.2016.
 */
public class VectorValue extends AbstractValue {
    Vector3D vector;

    public VectorValue(Vector3D vec)
    {
        vector = vec;
    }

    private VectorValue(double n)
    {
        double[] coords = {n};
        vector = new Vector3D(coords);
    }

    @Override
    public VectorValue add(AbstractValue operand) throws OperationNotSupportedException, DivisionByZeroException {
        return new VectorValue(vector.add(((VectorValue) operand).vector));
    }

    @Override
    public VectorValue sub(AbstractValue operand) throws OperationNotSupportedException, DivisionByZeroException {
        return new VectorValue(vector.sub(((VectorValue) operand).vector));
    }

    @Override
    public VectorValue mul(AbstractValue operand) throws OperationNotSupportedException, DivisionByZeroException {
        if (((VectorValue)operand).vector.dimension() == 1)
            return new VectorValue(vector.multiply(((VectorValue)operand).vector.getComponent(0)));
        else
            return new VectorValue(vector.scalar(((VectorValue)operand).vector));
    }

    @Override
    public AbstractValue div(AbstractValue operand) throws DivisionByZeroException, OperationNotSupportedException {
        throw new OperationNotSupportedException("Не знаю что значит поделить векторы");
    }

    @Override
    public String toString() {
        if (vector.dimension() == 1) return Double.toString(vector.getComponent(0));
        String returnedString = "{ ";
        for (int i = 0; i<vector.dimension()-1; i++)
            returnedString += Double.toString(vector.getComponent(i)) + ", ";
        returnedString += Double.toString(vector.getComponent(vector.dimension()-1));
        return returnedString + " }";
    }
}
