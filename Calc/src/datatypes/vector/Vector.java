package datatypes.vector;

import calculator.OperationNotSupportedException;

public interface Vector {
	 int dimension();             // размерность
	 double getComponent(int i);  // возвращает компоненту вектора
	 double scalar(Vector v) throws OperationNotSupportedException;
	 //double len();
	 double len();
	 Vector multiply(double factor) throws OperationNotSupportedException;
	 Vector add(Vector v) throws OperationNotSupportedException;
	 Vector sub(Vector v) throws OperationNotSupportedException;
}