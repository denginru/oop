/**
 * Created by roman on 17.04.16.
 */

import  calculator.*;
import datatypes.complex.ComplexValueParser;
import datatypes.integer.IntegerValueParser;
import datatypes.real.RealValueParser;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;


public class Solve {
    public static void main(String[] args)
    {
        IntegerValueParser intParser = new IntegerValueParser();

        Scanner in = new Scanner(System.in);
        PrintWriter out = new PrintWriter(System.out);
        AbstractValueParser parser;

        out.println("Chose datatype (type number):\n 1) Integer\n 2) Real \n 3) Complex");
        out.flush();

        try {
            int n = in.nextInt();
            in.nextLine();
            switch (n) {
                case 1:
                    parser = new IntegerValueParser();
                    break;
                case 2:
                    parser = new RealValueParser();
                    break;
                case 3:
                    parser = new ComplexValueParser();
                    break;
                default:
                    out.println("Bad number");
                    return;
            }
            Calculator calc = new Calculator(parser);
            try {
                out.println(calc.calculate(in.nextLine(), in.nextLine(), in.nextLine()));
            }
            catch (DivisionByZeroException ex)
            {
                out.println(ex.getMessage());
            }

            catch (OperationNotSupportedException ex)
            {
                out.println(ex.getMessage());
            }

            catch (ParseValueException ex)
            {
                out.println(ex.getMessage());
            }
        }

        catch (InputMismatchException ex)
        {
            out.print("Not a number");
        }
        out.flush();
    }
}
